package net.plings.android;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlingsFormatter {
	static String niceDate(String starts, String ends) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		try {
			Date s = sdf.parse(starts); 
			Date e = sdf.parse(ends);
			int thisYear = new Date().getYear();
			SimpleDateFormat startFormatter;
			SimpleDateFormat endFormatter;
			if (s.getYear() != thisYear || e.getYear() != thisYear) {
				startFormatter = new SimpleDateFormat("dd MMMM yyyy HH:mm");
				endFormatter = new SimpleDateFormat("dd MMMM yyyy HH:mm");
			}
			if (s.getMonth() == e.getMonth() && s.getDate() == e.getDate()) {
				startFormatter = new SimpleDateFormat("dd MMMM HH:mm");
				endFormatter = new SimpleDateFormat("HH:mm");
			}
			else {
				startFormatter = new SimpleDateFormat("dd MMMM HH:mm");
				endFormatter = new SimpleDateFormat("dd MMMM HH:mm");
			}
			return startFormatter.format(s) + " - " + endFormatter.format(e); 
		} catch (ParseException e) {
			return "Date error";
		}
	}
}
