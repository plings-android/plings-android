package net.plings.android;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class PlingsActivity extends Activity {
   int i = 0;
   HashMap<String,String> venue;
   HashMap<String,String> provider;
   HashMap<String,String> activity;
   HashMap<Integer, Pling> starredPlings;
   Pling pling;
   
   public String killNL(String text) {
	   if (text == null) return "null";
	   if (text.equals("\n") || text.equals("null\n")) return "";
	   else return text;
   }
	
   public void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
       setContentView(R.layout.activity);
   }
   
   public void onStart() {
	   super.onStart();
	   if (starredPlings == null) starredPlings = Plings.getStarredPlings();
       //TextView t = (TextView)findViewById(R.id.activitytext);
       Bundle extras = getIntent().getExtras();
       pling = (Pling)extras.getSerializable("pling");
       venue = pling.getVenue();
       provider = pling.getProvider();
       activity = pling.getActivity();
       
       Log.w("PlingsActivity", pling.getUpdated().toString());
	   
       String starchar = "";
       int id = Integer.parseInt(activity.get("id"));
       if (starredPlings != null) {
	       if (starredPlings.containsKey(id)) {
	    	   starchar = " ♥";
	       }
       }
       
       // TODO Use linked Activities to suggest future activities
       if (pling.getLinkedActivities() != null) {
    	   starchar += " ("+Integer.toString(pling.getLinkedActivities().size())+")";
       }
       
       ((TextView)findViewById(R.id.activity_name))
       		.setText(activity.get("Name")+starchar);
       ((TextView)findViewById(R.id.activity_date))
       		.setText(PlingsFormatter.niceDate(activity.get("Starts"), activity.get("Ends")));
       ((TextView)findViewById(R.id.activity_age))
    		.setText(activity.get("MinAge")+" - "+activity.get("MaxAge"));
       ((TextView)findViewById(R.id.activity_cost))
			.setText("£"+activity.get("Cost"));
       ((TextView)findViewById(R.id.activity_details))
			.setText(activity.get("Details"));
       ((TextView)findViewById(R.id.activity_contact))
			.setText(
				killNL(activity.get("ContactName")+"\n")+
				killNL(activity.get("ContactAddress")+"\n")+
				killNL(activity.get("ContactNumber")+"\n")+
				activity.get("ContactEmail")
			);
       ((TextView)findViewById(R.id.activity_location))
			.setText(
				killNL(venue.get("Name")+"\n")+
				killNL(venue.get("BuildingNameNo")+"\n")+
				killNL(venue.get("ContactStreet")+"\n")+
				killNL(venue.get("Town")+"\n")+
				killNL(venue.get("PostTown")+"\n")+
				killNL(venue.get("County")+"\n")+
				venue.get("Postcode")
			);
       ((TextView)findViewById(R.id.activity_organiser))
		.setText(
			killNL(provider.get("Name")+"\n")+
			killNL(provider.get("BuildingNameNo")+"\n")+
			killNL(provider.get("ContactStreet")+"\n")+
			killNL(provider.get("Town")+"\n")+
			killNL(provider.get("PostTown")+"\n")+
			killNL(provider.get("County")+"\n")+
			provider.get("Postcode")
		);
       ((TextView)findViewById(R.id.activity_network))
		.setText(
			killNL(provider.get("name")+"\n")+
			killNL(provider.get("description"))
		);
   }
   
   public String createText(HashMap<String,String> activity, String prefix) {
	   return createText(activity, prefix, new String[0]);
   }
   public String createText(HashMap<String,String> activity, String prefix, String[] ignoredKeys) {
	   Set<String> keys = activity.keySet();
	   String out = "\n";
	   HashSet<String> ignoredKeySet = new HashSet<String>(Arrays.asList(ignoredKeys));
	   for (String key: keys) {
		   if (!ignoredKeySet.contains(key)) {
			   out += prefix+key+":\n"+activity.get(key)+"\n\n";
		   }
	   }
	   return out;
   }
   
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.activity, menu);
	    return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
	    MenuItem i = menu.findItem(R.id.activity_menu_star);
        int id = Integer.parseInt(activity.get("id"));
	    if (starredPlings.containsKey(id)) {
	    	i.setTitle(R.string.unstar);
	    }
	    else {
	    	i.setTitle(R.string.star);
	    }
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
		    case R.id.activity_menu_map:
		    	String latlong = venue.get("Latitude")+","+venue.get("Longitude");
		    	String address = "";
		    	//if (mVenue.get("BuildingNameNo") != null) address += mVenue.get("BuildingNameNo")+", ";
		    	//if (mVenue.get("Street") != null) address += mVenue.get("Street")+", ";
		    	address += venue.get("Postcode");
		    	String uristring = "geo:"+latlong+"?z=14&q="+URLEncoder.encode(address);
		    	Log.w("PlingsActivity", uristring);
		    	Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uristring));
		        startActivity(mapIntent);
		        return true;
		    case R.id.activity_menu_email:
		    	Intent emailIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("mailto", activity.get("ContactEmail"), ""));
		        startActivity(emailIntent);
		        return true;
		    case R.id.activity_menu_phone:
		    	Intent phoneIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("tel", activity.get("ContactNumber"), ""));
		        startActivity(phoneIntent);
		        return true;
		    case R.id.activity_menu_info:
		    	Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.plings.net/a/"+activity.get("id")));
		        startActivity(webIntent);
		        return true;
		    case R.id.activity_menu_share:
		        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		        shareIntent.setType("text/plain");
		        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity.get("Name"));
		        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "http://www.plings.net/a/"+activity.get("id"));
		        startActivity(Intent.createChooser(shareIntent, "Share Activity"));
		        return true;
		    case R.id.activity_menu_calendar:
		    	Intent calendarIntent = new Intent(Intent.ACTION_EDIT);
		    	calendarIntent.setType("vnd.android.cursor.item/event");
		    	calendarIntent.putExtra("beginTime", activity.get("Starts"));
		    	calendarIntent.putExtra("endTime", activity.get("Ends"));
		    	calendarIntent.putExtra("title", activity.get("Name"));
		    	startActivity(calendarIntent);
		    	return true;
		    case R.id.activity_menu_star:
		        int id = Integer.parseInt(activity.get("id"));
		    	if (starredPlings.containsKey(id)) {
		    		starredPlings.remove(id);
		    	}
		    	else {
		    		Plings.starPlings(id, pling);
		    	}
		        String starchar = "";
			    if (starredPlings.containsKey(id)) {
				    starchar = " ♥";
			    }
		        ((TextView)findViewById(R.id.activity_name))
		        		.setText(activity.get("Name")+starchar);
		    	return true;
		    case R.id.activity_menu_plingback:
				Intent pbIntent = new Intent(this, PlingBack.class);
				pbIntent.putExtra("pling", pling);
				startActivity(pbIntent);
		    default:
		        return super.onOptionsItemSelected(item);
	    }
	}
}