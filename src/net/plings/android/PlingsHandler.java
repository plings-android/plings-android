package net.plings.android;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler; 

public class PlingsHandler extends DefaultHandler { 
	StringBuffer buff = null;
    boolean buffering = false;
    boolean header = true;
    boolean inVenue = false;
    boolean inProvider = false;
    boolean inLinkedActivities = false;
    boolean getProvider = true;
    List<Pling> plings;
    HashMap<String, String> currentActivity;
    HashMap<String, String> currentVenue;
    HashMap<String, String> currentProvider;
	private static final Set<String> ACTIVITYFIELDS = new HashSet<String>(Arrays.asList(
		     new String[] {"Name", "Starts", "Ends", "Details", "MinAge", "MaxAge", "Cost", "ProviderProjectDept", "ContactName", "ContactNumber", "ContactEmail", "ContactAddress", "linkedActivities"}
		));
	private static final Set<String> VENUEFIELDS = new HashSet<String>(Arrays.asList(
		     new String[] {"Name", "BuildingNameNo", "Street", "Town", "PostTown", "County", "Postcode", "Telephone", "PlingsPlacesLink", "LA", "Ward", "Latitude", "Longitude", "LAName", "WardName" }
		));
	private static final Set<String> PROVIDERFIELDS = new HashSet<String>(Arrays.asList(
		     new String[] {"Name", "Description", "Website", "Contact", "Email", "Phone", "Fax", "BuildingNameNo", "Street", "Town", "PostTown", "County", "Postcode", "LA", "Ward", "Latitude", "Longitude", "LAName", "WardName", "name", "description", "website" }
		));
	
    public PlingsHandler() {
    }
    
    public List<Pling> getPlings() {
    	return plings;
    }
    
    public void startDocument() throws SAXException {
        // Some sort of setting up work
    	plings = new ArrayList<Pling>();
    } 
    
    public void endDocument() throws SAXException {
        // Some sort of finishing up work
    } 
    
    public void startElement(String namespaceURI, String localName, String qName, 
            Attributes atts) throws SAXException {
    	if (!inLinkedActivities) {
	    	if (localName.equals("Results")) {
	            buff = new StringBuffer("");
	            buffering = true;
	    	}
	    	else if (localName.equals("plings")) {
	    		header = false;
	    	}
	    	else if (localName.equals("activity")) {
	    		currentActivity = new HashMap<String, String>();
	    		currentVenue = new HashMap<String, String>();
	    		if (getProvider) currentProvider = new HashMap<String, String>();
	    		currentActivity.put("id", atts.getValue("id"));
	    	}
	    	else if (localName.equals("venue")) {
	    		inVenue = true;
	    	}
	    	else if (localName.equals("provider")) {
	    		inProvider = true;
	    	}
	    	else if (localName.equals("linkedActivities")) {
	    		inLinkedActivities = true;
	    	}
	    	else if (ACTIVITYFIELDS.contains(localName) || VENUEFIELDS.contains(localName) || PROVIDERFIELDS.contains(localName)) {
	    		buff = new StringBuffer("");
	            buffering = true;
	    	}
    	}
    	else if (localName.equals("activity")) {
    		plings.add(new Pling(currentActivity, currentVenue, currentProvider));
    		currentActivity = new HashMap<String, String>();
    		currentVenue = new HashMap<String, String>();
    		currentActivity.put("id", atts.getValue("id"));
    		inLinkedActivities = false;
    		getProvider = false;
    	}
    } 
    
    public void characters(char ch[], int start, int length) {
        if (buffering) {
            buff.append(ch, start, length);
        }
    } 
    
    public void endElement(String namespaceURI, String localName, String qName) 
    throws SAXException {
    	if (!inLinkedActivities) {
	    	if (localName.equals("activity")) {
	    		plings.add(new Pling(currentActivity, currentVenue, currentProvider));
	    	}
	    	else if (localName.equals("venue")) {
	    		inVenue = false;
	    	}
	    	else if (localName.equals("provider")) {
	    		inProvider = false;
	    	}
	    	else if (inVenue && VENUEFIELDS.contains(localName)) {
	    		buffering = false;
	            String content = buff.toString();
	            content = content.trim();
	            currentVenue.put(localName, content);
	    	}
	    	else if (inProvider && PROVIDERFIELDS.contains(localName)) {
	    		buffering = false;
	            String content = buff.toString();
	            content = content.trim();
	            currentProvider.put(localName, content);
	    	}
	    	else if (ACTIVITYFIELDS.contains(localName)) {
	    		if (header) throw new SAXException();
	    		buffering = false;
	            String content = buff.toString();
	            content = content.trim();
	    		currentActivity.put(localName, content);
	    	}
    	}
    	else if (localName.equals("linkedActivities")) {
    		inLinkedActivities = false;
    	}
    }
}