package net.plings.android;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class PlingsList extends Activity {
	int radius = 3;
	String sortby = "starts";
	String display;
	List<Pling> futurePlings;
	
	private class DownloadDataTask extends AsyncTask<String, Integer, String> {	
	    private String networkFetch(String[] postcodes) {
	    	 String queryString;
	    	 if (postcodes[0].equals("no")) {
		         LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		         String provider = lm.getBestProvider(new Criteria(), true);
		         String latlong;
		         if (provider == null) {
		         	latlong = "52.4652559692,-1.94111588187";
		         	return "No valid location mProvider. Use postcode search instead.";
		         }
		         else {
		 	        Location location = lm.getLastKnownLocation(provider);
		 	        if (location == null) {
		 	        	latlong = "52.4652559692,-1.94111588187";
		 	        	return "Unable to detect location. Use postcode search instead.";
		 	        }
		 	        else {
		 	        	latlong = Double.toString(location.getLatitude())+","+Double.toString(location.getLongitude());
		 	        }
		         }
		     	
		         //String currentDateTimeString = new SimpleDateFormat("yyyy-MM-dd+HH:mm:ss").format(new Date());
		         //Toast.makeText(getApplicationContext(), "Test"+currentDateTimeString, Toast.LENGTH_LONG).show();
		         
		         queryString = "latlong/"+latlong+"/";
	    	 }
	    	 else {
	    		 queryString = "postcode/"+postcodes[0].replace(" ", "+")+"/";
	    	 }
	         //String queryString = "";
	    	 Log.w("Plings", sortby);
	         String queryArgs = "MaxResults=50&FutureOnly=1&sortby="+sortby+"&radius=" + Double.toString(((double)radius)*1.6);
	         //String queryArgs = "FutureOnly=1&sortby=starts";
	         String urlstring = "http://feeds.plings.net/xml.activity.php/1/"+queryString+"?APIKey="+Conf.PLINGS_APIKEY+"&"+queryArgs;
	         Log.w("PlingsTest", urlstring);
	         URL url;
	 		try {
	 			url = new URL(urlstring);
	 		} catch (MalformedURLException e2) {
	 			return "Bad url - did you enter a valid postcode?";
	 		}
	         InputStream inputStream;
	         try {
	 			inputStream = url.openStream();
	 		} catch (IOException e1) {
 				return "Network error - Check that you have internet enabled.";
	 		}
	         //Resources resources = getResources();
	         //InputStream inputStream = resources.openRawResource(R.raw.plings);
	        
	 		String xmlerror = "There was an error with the plings api, if this problem persists, please contact the developer.";
	 		try {
	 	        SAXParserFactory spf = SAXParserFactory.newInstance();
	 	        SAXParser sp = spf.newSAXParser();
	 	
	 	        XMLReader xr = sp.getXMLReader();
	 	        PlingsHandler plingsHandler = new PlingsHandler();
	 	        xr.setContentHandler(plingsHandler);
	 	        
	 	        xr.parse(new InputSource(inputStream));
	 	        plingList = plingsHandler.getPlings();
	         }
	         catch (SAXException e) { return xmlerror; }
	         catch (IOException e) { return xmlerror; }
	         catch (ParserConfigurationException e) { return xmlerror; }	         
	         
	         return "";
	    }
		
		protected String doInBackground(String... postcodes) {
	    	 if (display != null && display.equals("starred")) {
	    		 HashMap<Integer,Pling> sp = (HashMap<Integer, Pling>)Plings.getStarredPlings().clone();
	 			 plingList = new ArrayList<Pling>(sp.values());
	    		 Date now = new Date();
	    		 for (Pling pling : plingList) {
	    			 try {
	    				 if (pling.getStart().before(now)) {
	    					 List<Pling> linkedPlings = pling.getLinkedActivities();
	    					 if (linkedPlings != null) {
		    					 for (Pling p : linkedPlings) {
		    						 int id = Integer.parseInt(p.getActivity().get("id"));
		    						 if (p.getStart().after(now)) {
		    							 if (!Plings.getStarredPlings().containsKey(id)) {
			    							 View header = findViewById(R.id.fetch_future);
			    		    				 header.setVisibility(View.VISIBLE);
			    		    				 futurePlings.add(p);
		    							 }
		    							 break;
		    						 }
		    					 }
	    					 }
	    				 }
	    			 } catch (ParseException e) {
	    				 continue;
	    			 }
	    		 }
	    		 return "";
	    	 }
	    	 else {
	    		 return networkFetch(postcodes);
	    	 }
	     }

	     protected void onProgressUpdate(Integer... progress) {
	         //setProgressPercent(progress[0]);
	     }

	     protected void onPostExecute(String result) {
	         //////////////////////////// UI ///////////////////////////
	   	  
	    	 if (result == "") {
		         ListView lv = (ListView) findViewById(R.id.plingslist);
		         if (plingList.isEmpty()) result = "Sorry, no plings were found.";
		         else {
			         lv.setAdapter(new PlingArrayAdapter<String,String>(PlingsList.this, R.layout.list_item, plingList));
			         lv.setTextFilterEnabled(true);
			         
			         lv.setOnItemClickListener(new OnItemClickListener() {
			           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			         	   Intent intent = new Intent(view.getContext(), PlingsActivity.class);
			               Pling pling = plingList.get(Integer.parseInt(Long.toString(id)));
			               intent.putExtra("pling", pling);
			               startActivity(intent);
			           }
			         });
			         
			         ViewSwitcher v = (ViewSwitcher) findViewById(R.id.switcher);
			         v.showNext();
		         }
	    	 }
	    	 if (result != "") {
	    		ViewSwitcher v = (ViewSwitcher) findViewById(R.id.loading_switcher);
    	        v.showNext();
    	        TextView t = (TextView) findViewById(R.id.loading_message);
    	        t.setText(result);
	    	 }
	     }
	 }
	
    static List<Pling> plingList;
    
    public static List<Pling> getPlingList() {
    	return plingList;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        
        futurePlings = new ArrayList<Pling>();

		Button ff = (Button) findViewById(R.id.fetch_future);
		ff.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				View header = findViewById(R.id.fetch_future);
				header.setVisibility(View.GONE);
				ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.switcher);
    	        vs.showNext();
				for (Pling pling : futurePlings) {
					int id = Integer.parseInt(pling.getActivity().get("id"));
					Plings.starPlings(id, pling);
				}
				new DownloadDataTask().execute();
			}
		});
        
        Bundle extras = getIntent().getExtras();
        String postcode = extras.getString("postcode");
        radius = extras.getInt("radius");
        sortby = extras.getString("sortby");
        display = extras.getString("display");
        
        new DownloadDataTask().execute(postcode);
    }
    
    @Override
    public void onRestart() {
    	super.onRestart();
        ListView lv = (ListView) findViewById(R.id.plingslist);
        ((PlingArrayAdapter<String, String>) lv.getAdapter()).notifyDataSetChanged();
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.list, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
		    case R.id.menu_map:
		    	Intent intent = new Intent(getApplicationContext(), GMap.class);
		        startActivity(intent);
		    default:
		        return super.onOptionsItemSelected(item);
	    }
	}
}