package net.plings.android;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class Plings extends Activity {
	public static final String STARRED_FILENAME = "starred";
	public final Integer[] RADII = new Integer[] {1,2,3,5,10,20,30,50,100};
	public final String[] SORTBY = new String[] { "starts", "ends", "distance" };
	public static HashMap<Integer,Pling> starredPlings;
	
	private static class PostLikeTask extends AsyncTask<Integer, Void, Void> {
		@Override
		protected Void doInBackground(Integer... ids) {
			for (int id : ids) {
			    HttpClient httpclient = new DefaultHttpClient();
			    HttpPost httppost = new HttpPost("http://plingback.appspot.com/api/plingbacks");
		    	
				try {  
			         List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();  
			         nameValuePairs.add(new BasicNameValuePair("pling_id", Integer.toString(id)));  
			         nameValuePairs.add(new BasicNameValuePair("plingback_type", "net.plings.android"));
		        	 nameValuePairs.add(new BasicNameValuePair("feedback_attribute", "approval"));
		        	 nameValuePairs.add(new BasicNameValuePair("approval_value", "1"));
		        	 httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			         
			         HttpResponse response = httpclient.execute(httppost);
			         HttpEntity entity = response.getEntity();
			         InputStream content = entity.getContent();
			         InputStreamReader irs = new InputStreamReader(content);
			         BufferedReader br = new BufferedReader(irs);
					 while (true) {
						 String line = br.readLine();
						 if (line == null) break;
						 Log.w("Plings", line);
			         }
				} catch (ClientProtocolException e) {
			    	 Log.w("Plings", e.toString());
			     } catch (IOException e) {
			    	 Log.w("Plings", e.toString());
				 } 
			}
			return null;
		}
	}
	
	private static class DownloadDataTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... v) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 1);
			Date oldDate = cal.getTime();
			HashMap<Integer,Pling> sp = (HashMap<Integer, Pling>)starredPlings.clone();
			for (Pling pling : sp.values()) {
				if (pling.getUpdated().before(oldDate)) {
					pling.update();
				}
			}
			return null;
		}
	}
	
	public static void starPlings(int id, Pling pling) {
        new PostLikeTask().execute(id);
		Plings.getStarredPlings().put(id, pling);
	}
	public static HashMap<Integer,Pling> getStarredPlings() {
		if (starredPlings == null) starredPlings = new HashMap<Integer,Pling>();
		return starredPlings;
	}
	public static void updateStarredPlings(FileOutputStream fos) {
		ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(fos);
			out.writeObject(starredPlings);
			out.close();
			fos.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}
	public static void refreshStarredPlings() {
        new DownloadDataTask().execute();
	}
	
	/*
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {		
		savedInstanceState.putSerializable("starred", starredPlings);
		super.onSaveInstanceState(savedInstanceState);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("starred")) {
        	Plings.starredPlings = (HashMap<Integer, Integer>)savedInstanceState.getSerializable("starred");
        	if (Plings.starredPlings == null) {
        		Plings.starredPlings = new HashMap<Integer, Integer>();
        	}
        }
		else {
	    	Plings.starredPlings = new HashMap<Integer, Integer>();
	    }
	}
	*/
	
	
	@Override
	public void onDestroy() {
		try {
			Plings.updateStarredPlings(openFileOutput(Plings.STARRED_FILENAME, Context.MODE_PRIVATE));
		} catch (FileNotFoundException e) {
		}
		super.onDestroy();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
    	if (Plings.starredPlings == null) {
    		FileInputStream fis;
			try {
				fis = openFileInput(Plings.STARRED_FILENAME);
	    		ObjectInputStream in = new ObjectInputStream(fis);
	    		Plings.starredPlings = (HashMap<Integer,Pling>)in.readObject();
	    		in.close();
	    		Log.w("Plings", "starredPlings restored");
			} catch (FileNotFoundException e) {
	    		Plings.starredPlings = new HashMap<Integer,Pling>();
	    		Log.w("Plings", "starredPlings created");
			} catch (IOException e) {
				Log.w("Plings", "IOException");
			} catch (ClassNotFoundException e) {
				Log.w("Plings", "ClassNotFoundException");
			}
    	}
    	else {
    		Log.w("Plings", "starredPlings already exists!");
    	}

		Plings.refreshStarredPlings();
        
        Button gps = (Button) findViewById(R.id.button_gps);
        gps.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(), PlingsList.class);
				Spinner s1 = (Spinner) findViewById(R.id.radius_spinner);
				Spinner s2 = (Spinner) findViewById(R.id.sortby_spinner);
				intent.putExtra("radius", RADII[s1.getSelectedItemPosition()]);
				intent.putExtra("sortby", SORTBY[s2.getSelectedItemPosition()]);
		        intent.putExtra("postcode", "no");
				startActivity(intent);
			}
        });
        Button postcode = (Button) findViewById(R.id.button_postcode);
        postcode.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(), PlingsPostcode.class);
				Spinner s1 = (Spinner) findViewById(R.id.radius_spinner);
				Spinner s2 = (Spinner) findViewById(R.id.sortby_spinner);
				intent.putExtra("radius", RADII[s1.getSelectedItemPosition()]);
				intent.putExtra("sortby", SORTBY[s2.getSelectedItemPosition()]);
				startActivity(intent);
			}
        });
        Button starred = (Button) findViewById(R.id.button_starred);
        starred.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(), PlingsList.class);
				intent.putExtra("display", "starred");
				startActivity(intent);
			}
        });
        
        Spinner radiusSpinner = (Spinner) findViewById(R.id.radius_spinner);
        ArrayAdapter<Integer> adapter1 = new ArrayAdapter<Integer>(
                this, android.R.layout.simple_spinner_item, RADII);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        radiusSpinner.setAdapter(adapter1);
        radiusSpinner.setSelection(2);
        
        Spinner sortbySpinner = (Spinner) findViewById(R.id.sortby_spinner);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, new String[] {"Start","End","Distance"});
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortbySpinner.setAdapter(adapter2);
    }
}