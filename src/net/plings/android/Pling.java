package net.plings.android;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.util.Log;

public class Pling implements Serializable {
	private static final long serialVersionUID = -2165978752066594476L;
	HashMap<String,String> mActivity;
	HashMap<String,String> mVenue;
	HashMap<String,String> mProvider;
	List<Pling> linkedActivities;
	Date updated;
	
	public Date getStart() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.parse(mActivity.get("Starts"));
	}
	public Date getEnd() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.parse(mActivity.get("Ends"));
	}
	public List<Pling> getLinkedActivities() {
		return linkedActivities;
	}
	public HashMap<String,String> getActivity() {
		return mActivity;
	}
	public HashMap<String,String> getVenue() {
		return mVenue;
	}
	public HashMap<String,String> getProvider() {
		return mProvider;
	}
	public Date getUpdated() {
		return updated;
	}
	public void update() {
		String id = mActivity.get("id");
        String urlstring = "http://feeds.plings.net/activity.php/"+id+".xml?APIKey="+Conf.PLINGS_APIKEY;
        Log.w("PlingsTest", urlstring);
        URL url;
		try {
			url = new URL(urlstring);
		} catch (MalformedURLException e2) {
			return;
		}
        InputStream inputStream;
        try {
			inputStream = url.openStream();
		} catch (IOException e1) {
			return;
		}
		
		try {
	        SAXParserFactory spf = SAXParserFactory.newInstance();
	        SAXParser sp = spf.newSAXParser();
	
	        XMLReader xr = sp.getXMLReader();
	        PlingsHandler plingsHandler = new PlingsHandler();
	        xr.setContentHandler(plingsHandler);
	        
	        xr.parse(new InputSource(inputStream));
	        List<Pling> plingList = plingsHandler.getPlings();
	        if (plingList.size() > 0) {
		        Pling newPling = plingList.get(0);
		        mActivity = newPling.getActivity();
		        mVenue = newPling.getVenue();
		        mProvider = newPling.getProvider();
		        plingList.remove(0);
		        updated = new Date();
	        }
	        linkedActivities = plingList;
        }
        catch (SAXException e) { }
        catch (IOException e) { }
        catch (ParserConfigurationException e) { }
	}
	
	public Pling(HashMap<String,String> activity, HashMap<String,String> venue, HashMap<String,String> provider) {
		mActivity = activity;
		mVenue = venue;
		mProvider = provider;
		updated = new Date();
	}
}
