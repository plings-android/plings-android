package net.plings.android;

import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class GMap extends MapActivity {
    List<Pling> plingList;
	
	List<Overlay> mapOverlays;
	Drawable drawable;
	MyItemizedOverlay itemizedOverlay;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		
		MapView mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		
		mapOverlays = mapView.getOverlays();
		drawable = this.getResources().getDrawable(R.drawable.mappingicon_purple);
		
		plingList = PlingsList.getPlingList();
		itemizedOverlay = new MyItemizedOverlay(this, drawable);
		
		for (Pling pling : plingList) {
			//activity[0]
			String lat = pling.getVenue().get("Latitude");
			String lng = pling.getVenue().get("Longitude");
			String name = pling.getActivity().get("Name");
			GeoPoint point = new GeoPoint((int)(Double.parseDouble(lat)*1e6), (int)(Double.parseDouble(lng)*1e6));
			OverlayItem overlayitem = new OverlayItem(point, "", name);
			itemizedOverlay.addOverlay(overlayitem);
		}
		
		mapOverlays.add(itemizedOverlay);
		
		MapView map = (MapView) findViewById(R.id.mapview);
		MapController mc = map.getController();
		mc.zoomToSpan(itemizedOverlay.getLatSpanE6(), itemizedOverlay.getLonSpanE6());
		mc.setCenter(itemizedOverlay.getCenter());
	}
	
	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void launchPlingsActvitiy(int id) {
		Intent intent = new Intent(this, PlingsActivity.class);
		Pling pling = plingList.get(id);
		intent.putExtra("pling", pling);
        startActivity(intent);
	}
}