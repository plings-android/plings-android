package net.plings.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;

public class PlingsPostcode extends Activity {
	private int radius;
	private String sortby;

	public void formSubmit(Context context) {
		EditText e = (EditText) findViewById(R.id.input);
		Intent intent = new Intent(context, PlingsList.class);
		intent.putExtra("radius", radius);
		intent.putExtra("sortby", sortby);
        intent.putExtra("postcode", e.getText().toString());
		startActivity(intent);
	}
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postcode);
        
        Bundle extras = getIntent().getExtras();
        radius = extras.getInt("radius");
        sortby = extras.getString("sortby");

        Button go = (Button) findViewById(R.id.go);
        go.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				formSubmit(v.getContext());
			}
        });
        
        final EditText edittext = (EditText) findViewById(R.id.input);
        edittext.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            	if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
            		  formSubmit(v.getContext());
                      return true;
                }
                return false;
            }
        });
	}
}