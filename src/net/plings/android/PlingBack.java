package net.plings.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class PlingBack extends Activity {
	Pling pling;
	String[] ATTENDED = new String[] {"", "attended", "didNotAttend", "willAttend", "willNotAttend" };
	// http://plingback.appspot.com/api/plings/344075/ratings
	
	private class SubmitDataTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.switcher2);
	        vs.showNext();
		}

		@Override
		protected Void doInBackground(Void... v) {
			Spinner attended = (Spinner) findViewById(R.id.plingback_attended);
			Spinner rate = (Spinner) findViewById(R.id.plingback_rate);
			TextView comments = (TextView) findViewById(R.id.plingback_comments);
			
		    HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost("http://plingback.appspot.com/api/plingbacks");
		    //HttpPost httppost = new HttpPost("http://libreapps.com/socialwebb/");
		    
			try {  
		         List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();  
		         nameValuePairs.add(new BasicNameValuePair("pling_id", pling.getActivity().get("id")));  
		         nameValuePairs.add(new BasicNameValuePair("plingback_type", "net.plings.android"));
		         if (attended.getSelectedItemPosition() != 0) {
		        	 nameValuePairs.add(new BasicNameValuePair("feedback_attribute", "attendance"));
		        	 nameValuePairs.add(new BasicNameValuePair("attendance_value", ATTENDED[attended.getSelectedItemPosition()]));
		         }
		         if (rate.getSelectedItemPosition() != 0) {
		        	 nameValuePairs.add(new BasicNameValuePair("feedback_attribute", "rating"));
		        	 nameValuePairs.add(new BasicNameValuePair("rating_value", rate.getSelectedItem().toString()+"0"));
		         }
		         if (comments.getText().toString() != "") {
		        	 nameValuePairs.add(new BasicNameValuePair("feedback_attribute", "comment"));
		        	 nameValuePairs.add(new BasicNameValuePair("comment_value", comments.getText().toString()));
		         }
	        	 httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		         
		         HttpResponse response = httpclient.execute(httppost);
		         HttpEntity entity = response.getEntity();
		         InputStream content = entity.getContent();
		         InputStreamReader irs = new InputStreamReader(content);
		         BufferedReader br = new BufferedReader(irs);
				 while (true) {
					 String line = br.readLine();
					 if (line == null) break;
					 Log.w("Plings", line);
		         }
		         Log.w("Plings", entity.toString());
			} catch (ClientProtocolException e) {
		    	 Log.w("Plings", e.toString());
		     } catch (IOException e) {
		    	 Log.w("Plings", e.toString());
			 }  
			
			 return null;
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plingback);
        
        Bundle extras = getIntent().getExtras();
        pling = (Pling) extras.getSerializable("pling");
        
        Button submit = (Button) findViewById(R.id.plingback_submit);
        submit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//s1.getSelectedItemPosition()
				//s2.getSelectedItemPosition()
				ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.switcher);
		        vs.showNext();
				new SubmitDataTask().execute();
			}
        });
	}
}
